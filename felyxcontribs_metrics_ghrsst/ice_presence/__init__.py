# encoding: utf-8
"""
The ice_presence plugin
-----------------------

This module is a felyx plugin, designed to a Boolean value describing
whether or not any ice is present in the miniprod. The presence of any
sea ice will yield a positive response.

By default, only pixels within the boundary of the felyx_site will
be included in the calculation. Other filters are available through use of the
'must_have' argument.

.. :copyright: Copyright 2013 Pelamis Scientific Software Ltd.
.. :license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: David Poulter <david.poulter@pelamis.co.uk>
.. codeauthor:: David Poulter <david.poulter@pelamis.co.uk>
"""

from felyx_processor.basetypes import MINIPRODUCT
from felyx_processor.processors.plugins import PluginMetadata, operator
from felyx_processor.processors.plugins.baseplugin import \
    UnivariateStatisticPlugin


info = PluginMetadata(
    'ice_presence', 'IcePresence',
    ['has_task', 'perform', 'is_reducer', 'expects_uri']
)


class IcePresence(UnivariateStatisticPlugin):
    """A plugin to reveal if ice is present in a miniprod."""

    @staticmethod
    def has_task(task_id):
        """
        Return True is task_id is 'ice_presence'.

        :param str task_id: The id of the task to test for.
        :return: True if task_id matches this task.
        :rtype: bool
        """
        return task_id in ['ice_presence']

    @staticmethod
    @operator(
        'metadata', 1,
        {
            'type': list,
            'userDefined': False,
            'doc': 'A list of Data objects for analysis'
        },
        {
            'type': str,
            'userDefined': True,
            'doc': 'The name of the field to test for ice presence in'
        },
        {
            'type': bool,
            'userDefined': True,
            'doc': 'Limit analysis to pixels strictly within the felyx site, '
                   'False to analyse all pixels in the miniprod'
        },
        {
            'type': list,
            'userDefined': True,
            'doc': 'A list of restriction dictionary elements'
        }
    )
    def ice_presence(
            data_list,
            field="sea_ice_fraction",
            limit_to_site=True,
            must_have=None):
        """
        Returns boolean ice presence, for each input field.
        ---
        :param bool limit_to_site: Limit analysis to pixels strictly within the
          felyx site, False to analyse all pixels in the miniprod.
        :param list data_list: A list of Data objects for analysis.
        :param str field: The name of the field to test for ice presence in.
        :param list must_have: A list of restriction dictionary elements.
        :return: A Boolean value for each element in data_list
        :rtype: list
        """

        if not all([_.datatype == MINIPRODUCT for _ in data_list]):
            raise NotImplementedError(
                'This plugin only supports miniprods as input'
            )

        results = []
        for miniprod in data_list:
            value = bool(
                miniprod.content.read_values(
                    field, limit_to_site=limit_to_site,
                    must_have=must_have
                ).max()
            )
            results.append(value)

        return results
