# encoding: utf-8
"""
The cloudy plugin
-----------------

This module is a felyx plugin, designed to return the percentage of the
ocean pixels in the miniprod which are obscured by cloud. It will assume
that quality information is in the field 'proximity_confidence' or
'quality_level'. Land and ice pixels, if masked as such, will not feature
in the evaluation of the result. Therefore, a miniprod will ALL land pixels
obscured by cloud, and ALL ocean pixels free of cloud, will be evaluated as
having 0% cloud cover.

By default, only pixels within the boundary of the felyx_site will
be included in the calculation. Other filters are available through use of the
'must_have' argument.

It utilises the UnivariateStatisticPlugin as it accepts only a single
field for analysis, and returns only a single value per input field.

.. :copyright: Copyright 2013 Pelamis Scientific Software Ltd.
.. :license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: David Poulter <david.poulter@pelamis.co.uk>
.. codeauthor:: David Poulter <david.poulter@pelamis.co.uk>
"""

import numpy

from felyx_processor.basetypes import MINIPRODUCT, MINIPROD_CONTENT_MASK
from felyx_processor.processors.plugins import PluginMetadata, operator
from felyx_processor.processors.plugins.baseplugin import \
    UnivariateStatisticPlugin

info = PluginMetadata(
    'cloudy', 'Cloudy',
    ['has_task', 'perform', 'is_reducer', 'expects_uri']
)


class Cloudy(UnivariateStatisticPlugin):
    """
    Plugin to return the percentage of cloud cover in a miniprod.
    """

    @staticmethod
    def has_task(task_id):
        """
        Return True if task_id is 'cloudy'.

        :param str task_id: The id of the task to test for.
        :return: True if task_id matches this task.
        :rtype: bool
        """
        return task_id in ['cloudy']

    @staticmethod
    @operator(
        'metadata', 1,
        {'type': list, 'userDefined': False,
         'doc': 'A list of Data objects for analysis'},
        {'type': int, 'userDefined': True,
         'doc': 'The confidence level below which data is assumed to be '
                'cloudy'},
        {'type': bool, 'userDefined': True,
         'doc': 'Limit analysis to pixels strictly within the '
                'felyx site, False to analyse all pixels in the miniprod'},
        {'type': list, 'userDefined': True,
         'doc': 'A list of restriction dictionary elements'}
    )
    def cloudy(
            data_list, confidence_value=4,
            limit_to_site=True, must_have=None):
        """
        Return the percentage of cloud cover over ocean pixels, filtered
        by the optional 'must_have' criteria.
        ---
        :param list must_have: A list of restriction dictionary elements.
        :param bool limit_to_site: Limit analysis to pixels strictly within the
          felyx site, False to analyse all pixels in the miniprod.
        :param int confidence_value: The confidence level below which data is
          assumed to be cloudy.
        :param list data_list: A list of Data objects for analysis.
        :return: A numerical value for each element in data_list
        :rtype: list
        """
        if not all([_.datatype == MINIPRODUCT for _ in data_list]):
            raise NotImplementedError(
                'This plugin only supports miniprods as input'
            )

        results = []
        for miniprod in data_list:

            if not MINIPROD_CONTENT_MASK in \
                    miniprod.content.get_fieldnames():
                # Product is not a felyx miniprod, as it doesn't have a
                # mask to define the limits of the felyx site.
                results.append(None)
                continue

            if "proximity_confidence" in miniprod.content.get_fieldnames():
                # Read the confidence flag
                confidence_values = miniprod.content.read_values(
                    'proximity_confidence',
                    limit_to_site=limit_to_site,
                    must_have=must_have
                ).flatten()
                number_of_pixels = confidence_values.size

                if isinstance(confidence_values.all(),
                              numpy.ma.core.MaskedConstant):
                    # All confidence values are masked, there is 0%
                    # confidence in any pixel in the scene, all pixels
                    # are cloudy.
                    result = 100.0
                else:
                    if isinstance(confidence_values.mask, numpy.bool_):
                        unprocessed = 0
                        possible_cloud = float(
                            numpy.sum(
                                numpy.bincount(
                                    confidence_values, minlength=6
                                )[0:confidence_value]
                            )
                        )
                    else:
                        unprocessed = \
                            numpy.bincount(confidence_values.mask)[1]
                        possible_cloud = float(
                            numpy.sum(
                                numpy.bincount(
                                    confidence_values[
                                        confidence_values.mask == False],
                                    minlength=6
                                )[0:confidence_value]
                            )
                        )
                    possible_cloud += float(unprocessed)
                    result = float(
                        (possible_cloud / number_of_pixels) * 100
                    )

            elif "quality_level" in miniprod.content.get_fieldnames():
                quality_level = miniprod.content.read_values(
                    'quality_level').flatten()
                number_of_pixels = quality_level.size

                if isinstance(quality_level.all(),
                              numpy.ma.core.MaskedConstant):
                    result = 100.0
                else:
                    if isinstance(quality_level.mask, numpy.bool_):
                        unprocessed = 0
                        possible_cloud = float(
                            numpy.sum(
                                numpy.bincount(
                                    quality_level, minlength=6
                                )[0:confidence_value]
                            )
                        )
                    else:
                        unprocessed = numpy.bincount(quality_level.mask)[1]
                        possible_cloud = float(
                            numpy.sum(
                                numpy.bincount(
                                    quality_level[
                                        quality_level.mask == False
                                    ], minlength=6
                                )[0:confidence_value]
                            )
                        )
                    possible_cloud += float(unprocessed)
                    result = float(
                        (possible_cloud / number_of_pixels) * 100
                    )
            else:
                result = None

            results.append(result)

        return results
